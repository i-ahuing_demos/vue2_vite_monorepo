import { fileURLToPath, URL, } from 'node:url'

import fs from 'fs-extra'
import path from 'path'
import AutoImort from 'unplugin-auto-import/vite'
import AutoImortVueComponent from 'unplugin-vue-components/vite'
import VueMacros from 'unplugin-vue-macros/vite'
import { defineConfig, } from 'vite'

import config from './vue-macros.config'
// https://vitejs.dev/config/
export default defineConfig(({ command, }) => {
  const outDir = path.resolve(process.cwd(), '../../../vue2_vite_monorepo_dist/static/demo')
  const filename = path.resolve(process.cwd(), '../../../vue2_vite_monorepo_dist/demo/views/home/home.html')
  const base = command == 'serve' ? '' : '/static/demo'
  return {
    base,
    build: {
      assetsDir: './',
      emptyOutDir: true,
      outDir,
      rollupOptions: {
        output: {
          assetFileNames: (file) => {
            const ext = path.extname(file.name)
            if (/\.(jpe?g|png|gif|webp|bmp)$/.test(ext)) {
              return 'img/[name].[hash].[ext]'
            }
            return '[ext]/[name]-[hash].[ext]'
          },
          chunkFileNames: 'js/[name]-[hash].js',
          entryFileNames: 'js/[name]-[hash].js',
        },
      },
    },
    esbuild: { jsx: 'preserve', },
    plugins: [
      VueMacros(config),
      // 自动导入
      AutoImort({
        dirs: [ './src/hooks/*.js', ],
        eslintrc: {
          enabled: true,
          filepath: './.eslintrc-auto-import.json',
        },
        exclude: [
          /[\\/]node_modules(?!\/test)[\\/]/,
          /[\\/]\.git[\\/]/,
          /[\\/]\.nuxt[\\/]/,
        ],
        imports: [
          'vue',
          'vue-router',
          'pinia',
        ],
        include: [
          /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
          /\.vue$/,
          /\.vue\?vue/, // .vue
        ],
        vueTemplate: true,
      }),
      AutoImortVueComponent({
        allowOverrides: true,
        directives: false,
        dirs: [ './src/components/', './src/components2/', ],
        dts: false,
        extensions: [ 'vue', ],
        include: [
          /\.vue$/,
          /\.vue\?vue/,
          /\.js$/,
        ],
        version: 2.7,
      }),

      // 手动处理index.html
      {
        closeBundle() {
          fs.moveSync(
            path.join(outDir, 'index.html'), filename, { overwrite: true, }
          )
        },
        name: 'buildHtml',
      },
    ],
    resolve: { alias: { '@': fileURLToPath(new URL('./src', import.meta.url)), }, },
  }
})
