// @ts-check
import Vue from '@vitejs/plugin-vue2'
import VueJsx from '@vitejs/plugin-vue2-jsx'
/** @type {import('unplugin-vue-macros').Options} */
export default {
  booleanProp: true,

  defineProp: { edition: 'johnsonEdition', },
  exportExpose: { include: [ /export-expose.*\.vue$/, ], },
  exportProps: { include: [ /export-props.*\.vue$/, ], },

  exportRender: { include: [ /export-render.*\.vue$/, ], },

  namedTemplate: false,
  plugins: {
    vue: Vue({ include: [ /\.vue$/, /\.setup\.[cm]?[jt]sx?$/, ], }),
    vueJsx: VueJsx(),
  },
  setupBlock: true,

  setupSFC: true,
}