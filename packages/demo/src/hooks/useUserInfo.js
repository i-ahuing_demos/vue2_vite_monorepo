export const useUserInfo = defineStore('userInfo', {
  actions: {
    setCount() {
      this.count++
    },
    setUserInfo(user) {
      this.userInfo = {
        ...this.userInfo,
        ...user,
      }
    },
  },
  state: () => ({
    count: 0,
    userInfo: {
      id: 1,
      name: '小曾',
      role: '普通用户',
    },
  }),
})