const state = reactive({
  count: 0,
  userInfo: {
    id: 1,
    name: '小曾',
    role: '普通用户',

  },
})
export default () => {
  const setUserInfo = (user) => {
    state.userInfo = {
      ...state.userInfo,
      ...user,
    }
  }
  const setCount = () => {
    state.count++
  }
  return {
    setCount,
    setUserInfo,
    state,
  }
}