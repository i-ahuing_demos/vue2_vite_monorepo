import './assets/main.css'

import { createPinia, PiniaVuePlugin, } from 'pinia'
import Vue from 'vue'

import App from './App.vue'
import router from './router'

Vue.use(PiniaVuePlugin)
const pinia = createPinia()
Vue.config.productionTip = false
new Vue({
  router,
  name: 'Root',
  // store,
  pinia,
  render: h => h(App),
}).$mount('#app')
